<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ApiResponser;

    public function register(Request $request)
    {

       
        $validator = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users,email',
            'phone' => 'required|numeric|max:15',
            'gender' => 'required|string|max:10',
            'password' => 'required|string|min:6|confirmed'
        ]);
        // if ($validator->fails()){
        //     $messages = $validator->messages();
        //     return $this->error($messages, 401);
        // }

        $user = User::create([
            'first_name' => $validator['first_name'],
            'last_name' => $validator['last_name'],
            'phone' => $validator['phone'],
            'gender' => $validator['gender'],
            'email' => $validator['email'],
            'password' => bcrypt($validator['password'])
            
        ]);

        return $this->success([
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }

    public function login(Request $request)
    {
        $attr = $request->validate([
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6'
        ]);

        if (!Auth::attempt($attr)) {
            return $this->error('Credentials not match', 401);
        }

        return $this->success([
            'token' => auth()->user()->createToken('API Token')->plainTextToken
        ]);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'Tokens Revoked'
        ];
    }
}