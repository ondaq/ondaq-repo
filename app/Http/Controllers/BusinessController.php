<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Service;
use App\Traits\ApiResponser;

class BusinessController extends Controller
{
	use ApiResponser;
	public $category;
	public function __construct(){
   		$this->category = new Category;
	}
    public function getCategoires(){
   		return $this->success([
          Category::with('services')->where('status',1)->get()
        ]);
    }
    public function getServices($cate_id=""){
    	if (!empty($cate_id)) {
    		$services = Service::with('category')->where('category_id',$cate_id)->where('status',1)->get();
    	}else{
    		$services = Service::with('category')->where('status',1)->get();
    	}
   		return $this->success([
          $services
        ]);
    }
}
