<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    // protected $table = 'categories';

    public function services(){
        return $this->hasMany('App\Models\Service');
    }

    // public function getAllCategories(){
    //     $this->where('status',1)->orderBy('title', 'asc')->get();
    // }

}
