import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import bootstrap from 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'                      



import { library } from '@fortawesome/fontawesome-svg-core'
import { faSignOutAlt,faLongArrowAltRight,faStar,faStarHalfAlt,faMapMarkerAlt,faPhoneAlt,faCheck,faLeaf,
        faAngleRight,faLocationArrow,faArrowRight,faShare,faHeart,faSlidersH} 
        from '@fortawesome/free-solid-svg-icons'
import { faFacebookF,faYoutube, faLinkedinIn, faTwitter } from '@fortawesome/free-brands-svg-icons'
import { faBell,faClock,faMap } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faFacebookF,faYoutube,faLinkedinIn,faTwitter,faSignOutAlt,faBell,faLongArrowAltRight,faStar,faStarHalfAlt,
    faMapMarkerAlt,faPhoneAlt,faClock,faCheck,faLeaf,faAngleRight,faLocationArrow,faArrowRight,faShare,faHeart,faSlidersH,faMap
    )


import '@/assets/css/style.css'

window.$ = window.jQuery = require('jquery')

import axios from 'axios'
axios.defaults.baseURL = 'http://127.0.0.1:8000/api/';
axios.defaults.headers.common['Authorization'] = localStorage.getItem('token')


createApp(App)
.use(store)
.use(router)
.use(bootstrap)
.component('fa', FontAwesomeIcon)
.mount('#app')
