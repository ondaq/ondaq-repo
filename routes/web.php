<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontendController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::get('/{any}', [FrontendController::class, 'app'])->where('any', '^(?!api).*$');

// For admin application
//Route::get('/admin{any}', 'FrontendController@admin')->where('any', '.*');
// For public application
//Route::any('/{any}', 'FrontendController@app')->where('any', '^(?!api).*$');